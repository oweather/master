<?php

##### GLOBAL

    #date_default_timezone_set('America/Costa_Rica');
    #$fdate = date("h:ia / d-m-Y"); /*fecha*/
                   
    define("lhost",$_SERVER['REMOTE_ADDR']=="127.0.0.1",true);

##### TOOLS


// niceShow
function dump($data) {
    if(is_array($data)) { //If the given variable is an array, print using the print_r function.
        print "<pre>-----------------------\n";
        print_r($data);
        print "-----------------------</pre>";
    } elseif (is_object($data)) {
        print "<pre>==========================\n";
        var_dump($data);
        print "===========================</pre>";
    } else {
        print "=========&gt; ";
        var_dump($data);
        print " &lt;=========";
    }
}

// Encode CSV Field
function EncodeCSVField($string) {
    if(strpos($string, ',') !== false || strpos($string, '"') !== false || strpos($string, "\n") !== false) {
        $string = '"' . str_replace('"', '""', $string) . '"';
    }
    return $string;
}

// Secure all $_GET variables
function sGet($string){#return functios 1st time check params
        $string = $_GET[$string];
        trim($string);
        $badWords = array("%", "<", ">", "*", ";", "(", ")", "'", '"', " ", "=", "&", "@", "$", "!", "#");
        $string = str_replace($badWords, "", $string);
        //$string = mysql_escape_string($string);
          return $string;
        #old : return addslashes($GetVar);
}



?>