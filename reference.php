<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Weather CSV Reference</title>

    <style>
        body {
            background-color: #333;
            font-size: 12px;
        }
        * {
            color: #ccc;
            font-family: Arial;

        }
        b,a {
            color: cyan;
        }
    </style>
    
</head>
<body>
	<div style="margin-left: 20px !important;">
    
        <h1>Weather CSV Reference</h1>

        <li>Temporary URL service : <a href="http://191.238.228.32/" target="_blank">http://191.238.228.32/</a></li>
        <li>Inside the <b>cities.json</b> file, you will find the list of cities</li>
        <li>The file returned is named <b>weather.csv</b></li>
        <li>Exists two parameters: 
            <br>
            <div style="padding-left: 150px;">
                file = false : indicates that no will download a file, only return the data <br>
                titles = false : indicates that no include the header titles, only return the data <br>
                <br>
                ej: <a href="http://191.238.228.32/?file=false&titles=false" target="_blank">http://191.238.228.32/?file=false&titles=false</a>
            </div>
            <br>
        </li>
        <li>Parsing CSV with PHP: 
            <br>
            <div style="font-family: consolas; padding-left: 150px;">
                $csv = array_map('str_getcsv', file('http://191.238.228.32/?file=false&titles=false')); <br>
                dump($csv);
            </div>
            <br>
        </li>
        <li>Example returned array:</li>
        
        <?php

            // niceShow
            function dump($data) {
                if(is_array($data)) { //If the given variable is an array, print using the print_r function.
                    print "<pre>-----------------------\n";
                    print_r($data);
                    print "-----------------------</pre>";
                } elseif (is_object($data)) {
                    print "<pre>==========================\n";
                    var_dump($data);
                    print "===========================</pre>";
                } else {
                    print "=========&gt; ";
                    var_dump($data);
                    print " &lt;=========";
                }
            }

            $csv = array_map('str_getcsv', file('http://191.238.228.32/?file=false&titles=false'));
            dump($csv);

            /*
            $csvData = file_get_contents("http://191.238.228.32/?file=false&titles=false");
            $lines = explode(PHP_EOL, $csvData);
            $array = array();
            foreach ($lines as $line) {
                $array[] = str_getcsv($line);
            }

            //print_r($array);
            dump($array);
            */
        ?>

    </div>

</body>
</html>