<?php
error_reporting(E_ERROR);
ini_set("include_path","includes/");
include_once("header.inc.php");

/*
 * Weather CSV 
 */

/* 
 * @params 
 */

$return_file = "true";
$include_titles = "true";
$titles = array('Unique_Id','Reporting_Label','Is_default','City','Weather');
#$filename = 'weather_csv_' . date('Ymd') .'-' . date('His');
$filename = 'weather';
#$possible_actions = array("file","titles","city","code");

// Weather Conditions Code
$rainyArray = array(803,804,900,901,902,903,905,906,956,957,958,959,960,961,962);
$snowdyArray = array(903,905);
$sunnyArray = array(803,905,951,952,953,954,955,956);

/* ------------------------------------------------------------- */

// Actions
if(isset($_GET["file"])){$return_file = sGET("file");}
if(isset($_GET["titles"])){$include_titles = sGET("titles");}

// Get Weather From Cities
$cities_source = file_get_contents("./cities.json");
$cities = json_decode($cities_source,true);

$weatherIDs = array();

$weatherIDs[ ] = array( // Generic setup first row
						"UNIQUE_ID" => 1,
						"REPORTING_LABEL" => "Generic",
						"IS_DEFAULT" => "TRUE",
						"CITY" => "",
						"WEATHER" => "generic"
					);

$i=2;
foreach ($cities as $city){
	$weatherCode = ""; 
	$string = file_get_contents("http://api.openweathermap.org/data/2.5/weather?q=".$city);
	$string_decode = json_decode($string,true);
	$code = $string_decode["weather"][0]["id"];
	if($code>=200 && $code<=500 || in_array($code, $rainyArray)){
	    $weatherCode = 'rainy';
	}else if($code>=600 && $code<=622 || in_array($code, $snowdyArray)){
	    $weatherCode = 'snow';
	}else if(in_array($code, $sunnyArray)){
	    $weatherCode = 'sunny';
	}else{
	    $weatherCode = 'sunny';
	}

	$weatherIDs[ ] = array( 
						//"ID" => $string_decode["weather"][0]["id"],
						"UNIQUE_ID" => $i,
						"REPORTING_LABEL" => $string_decode["name"],
						"IS_DEFAULT" => "FALSE",
						"CITY" => $string_decode["name"],
						"WEATHER" => $weatherCode
					);
	$i++;
} //dump($weatherIDs);

/*
 * CSV Formater
 */

$fh = fopen('php://output', 'w');
ob_start();
if($include_titles==="true"){fputcsv($fh,$titles);} // including headers
for($i=0;$i<=count($weatherIDs);$i++){fputcsv($fh,$weatherIDs[$i]);} // including data
$string = ob_get_clean();

/* 
 * Output CSV
 */

if($return_file==="true"){
	header('Pragma: public');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0, no-cache, no-store');
	header('Cache-Control: private', false);
	header('Content-Type: text/csv');
	//header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename="' . $filename . '.csv";');
	header('Content-Transfer-Encoding: binary');

	exit($string);
}else{
	echo $string;
	exit();
}

?>